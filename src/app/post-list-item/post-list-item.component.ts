import {Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';

import {Post} from '../models/post.model';
import {PostService} from '../services/post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post: Post;
  @Input() idx: number;

  constructor(private postService: PostService, private router: Router ) {
  }

  ngOnInit() {
  }

  onLove(how) {
    if (how) {
      this.post.loveIts++;
    } else {
      this.post.loveIts--;
    }
    this.postService.savePosts();
  }

  onDelete() {
    this.postService.removePost(this.post);
  }

  onView() {
    this.router.navigate(['/posts', 'view', this.idx]);
  }

}
