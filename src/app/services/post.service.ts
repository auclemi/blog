import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';
import * as _ from 'lodash';

import {Post} from '../models/post.model';

@Injectable()
export class PostService {

  posts: Post[] = [];
  postSubject = new Subject<Post[]>();

  constructor() {

  }

  emitPosts() {
    this.postSubject.next(this.posts);
  }

  savePosts() {
    this.posts = _.orderBy(this.posts, ['date_at'], ['desc']);
    firebase.database()
      .ref('/posts')
      .set(this.posts)
      .catch(reason => {
        console.log('reason', reason);
      });
  }

  getPosts() {
    firebase.database()
      .ref('/posts')
      .on('value', (data: any) => {
          this.posts = data.val() ? data.val() : [];
          this.posts = _.orderBy(this.posts, ['created_at'], ['desc']);
          this.emitPosts();
        }
      );
  }

  getSinglePost(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/posts/' + id).once('value').then(
          (data: any) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }

  createNewPost(newPost: Post) {
    newPost.loveIts = 0;
    newPost.created_at = new Date().toISOString();
    this.posts.push(newPost);
    this.savePosts();
    this.emitPosts();
  }

  removePost(post: Post) {
    const postIndexToRemove = this.posts.findIndex(
      (postEl) => {
        if (postEl === post) {
          return true;
        }
      }
    );
    this.posts.splice(postIndexToRemove, 1);
    this.savePosts();
    this.emitPosts();
  }
}
