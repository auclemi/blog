export class Post {
  title: string;
  content: string;
  loveIts: number;
  created_at: string;

  constructor(title, content){
    this.title = title;
    this.content = content;
  }
}
