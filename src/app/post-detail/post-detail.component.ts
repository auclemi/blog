import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Post} from '../models/post.model';
import {PostService} from '../services/post.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {
  post: Post;

  constructor(private route: ActivatedRoute, private postService: PostService,
              private router: Router) {

  }

  ngOnInit() {
    // this.post = {};
    this.post = new Post('', '');
    const id = this.route.snapshot.params['id'];
    this.postService.getSinglePost(+id).then(
      (post: Post) => {
        this.post = post;
      }
    );
  }

  onReturn() {
    this.router.navigate(['/posts']);
  }

  onLove(how) {
    if (how) {
      this.post.loveIts++;
    } else {
      this.post.loveIts--;
    }
    console.log(this.post)
    // this.postService.savePosts();
  }

}
