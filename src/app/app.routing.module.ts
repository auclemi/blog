import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {PostListComponent} from './post-list/post-list.component';
import {NewPostComponent} from './new-post/new-post.component';
import {PostDetailComponent} from './post-detail/post-detail.component';

const appRoutes: Routes = [
  {path: 'posts', component: PostListComponent},
  {path: 'new-post', component: NewPostComponent},
  {path: 'posts/view/:id', component: PostDetailComponent},
  {path: '', redirectTo: 'posts', pathMatch: 'full'},
  {path: '**', redirectTo: 'posts'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
