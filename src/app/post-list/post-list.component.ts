import {Component, OnInit, Input} from '@angular/core';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';


import {Post} from '../models/post.model';
import {PostService} from '../services/post.service';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  @Input() posts: Post[];
  postSubscription: Subscription;

  constructor(private postService: PostService) {
  }

  ngOnInit() {
    this.postSubscription = this.postService.postSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postService.getPosts();
  }

  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }

}
