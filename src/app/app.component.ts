import {Component} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  blogTitle = 'My blog';

  constructor() {
    const config = {
      apiKey: 'AIzaSyBt0_5IjfMA65Oaf03wbWbWEPVA-tcrcE4',
      authDomain: 'blog-ab064.firebaseapp.com',
      databaseURL: 'https://blog-ab064.firebaseio.com',
      projectId: 'blog-ab064',
      storageBucket: 'blog-ab064.appspot.com',
      messagingSenderId: '1063431358666'
    };
    firebase.initializeApp(config);
  }
}
